<?php

namespace Drupal\tests\carryquery\Unit;

use Drupal\carryquery\PathProcessor\CarryQueryPathProcessor;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tests query params processing for carry forward.
 *
 * @coversDefaultClass Drupal\carryquery\PathProcessor\CarryQueryPathProcessor
 * @group PathProcessor
 */
class CarryQueryPathProcessorTest extends UnitTestCase {

  /**
   * Tests porcessoutbound functionality.
   *
   * @covers ::processOutbound
   * @dataProvider providerProcessOutbound
   */
  public function testProcessOutbound($path, $expected, $parameters) {
    $config_factory = $this->prophesize(ConfigFactory::class);
    $config = $this->prophesize(Config::class);
    $config->get('keys')->willReturn([]);
    $config_factory->get('carryquery.settings')->willReturn($config->reveal());
    $request = Request::create($path, 'GET', $parameters);
    $processor = new CarryQueryPathProcessor($config_factory->reveal());
    $options = [];
    $this->assertNotNull($request);
    $this->assertArraySubset(['data'], $request->query->keys());
    $this->assertEquals($expected, $processor->processOutbound($path, $options, $request));
  }

  /**
   * Outbound path processor actual and expected data.
   */
  public function providerProcessOutbound() {
    return [
      ['/', '/', ['data' => 'value']],
      ['/user', '/user', ['data' => 'value']],
    ];
  }

}

# Carry URL address query params across pages

This module provides URL address query carry forward functionality from page to
next page without any Javascript or Jquery code.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/carryquery).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/carryquery).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Token](https://www.drupal.org/project/token)
- [Token Filter](https://www.drupal.org/project/token_filter)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Add the configuration in *Configuration -> Search and metadata -> Carry query*


## Maintainers

- Sainath Badiger - [sainathdb](https://www.drupal.org/u/sainathdb)
- Swetha Bapanah K S - [Swetha_Bapanah](https://www.drupal.org/u/swetha_bapanah)

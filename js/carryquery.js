/**
 * @file
 * Attaches behaviors for the carryquery module.
 */
(function($, Drupal, drupalSettings) {
    'use strict';

    Drupal.behaviors.carryquery = {
        attach: function(context, settings) {
            var settings = drupalSettings.carryquery;
            if (settings.js) {
                // Reference: https://www.analyticsmania.com/post/transfer-utm-parameters-google-tag-manager/
                var keys = settings.keys;
                var host = location.host;
                var currentParams = location.search;
                if (currentParams.length) {
                    var links = $(context).find('a');
                    links.once('carryquery').each(function(linkIndex) {
                        var href = links[linkIndex].href;
                        if (href.indexOf(host) > -1 && href.indexOf("#") === -1) {
                            links[linkIndex].href = addParams(href, keys, currentParams);
                        }
                    });
                }
            }
        }
    };

    function addParams(url, keys, currentParams) {
        var newUrl = (url.indexOf('?') === -1) ? (url + '?') : (url + '&');
        var paramsToAdd = [];
        for (var keysIndex = 0; keysIndex < keys.length; keysIndex++) {
            if (newUrl.indexOf(keys[keysIndex]) === -1) {
                var paramValue = getParam(keys[keysIndex], currentParams);
                if (paramValue.length) {
                    paramsToAdd.push(keys[keysIndex] + '=' + paramValue)
                }
            }
        }
        return newUrl + paramsToAdd.join('&');
    }

    function getParam(name, currentParams) {
        if (name = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)')).exec(currentParams)) {
            return decodeURIComponent(name[1]);
        }
        return '';
    }
 })(jQuery, Drupal, drupalSettings);

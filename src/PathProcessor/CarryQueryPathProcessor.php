<?php

namespace Drupal\carryquery\PathProcessor;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Symfony\Component\HttpFoundation\Request;

/**
 * Processes the outbound path and adds the query parameters to it.
 */
class CarryQueryPathProcessor implements OutboundPathProcessorInterface {

  /**
   * A config factory for retrieving required configuration.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs a CarryQueryPathProcessor object.
   *
   * @param Drupal\Core\Config\ConfigFactory $configFactory
   *   A config factory for retrieving carry forward query information.
   */
  public function __construct(ConfigFactory $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    // Get the carry query configuration settings.
    $config = $this->configFactory->get('carryquery.settings');

    $js = $config->get('js');
    if ($js) {
      return $path;
    }

    $keys = $config->get('keys');
    $inkeys = $request->query->keys();
    // Get the common keys from both keys stored in the configuration and
    // kyes of query address parameters.
    if (is_array($keys) && is_array($inkeys)) {
      $commonkeys = array_intersect($keys, $inkeys);
    }
    // Add the cache context for query arguments.
    if ($bubbleable_metadata) {
      $bubbleable_metadata->addCacheContexts(['url.query_args']);
    }

    if (isset($commonkeys)) {
      foreach ($commonkeys as $value) {
        $options['query'][$value] = $request->query->get($value);
      }
    }
    return $path;
  }

}

<?php

namespace Drupal\carryquery\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure the query carry forward keys and related information.
 */
class QredirectConfig extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['carryquery.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'carryquery';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('carryquery.settings');

    $form['carryqueryconfig'] = [
      '#title' => $this->t('Query carry forward configuration'),
      '#type' => 'textarea',
      '#default_value' => !empty($config->get('carryqueryconfig')) ? $config->get('carryqueryconfig') : '',
      '#description' => $this->t(
          'Add the URL query parameter key here to carry forward them to next page. Example: URL contains path /path1?data=value and you want to carry data=value to any of the next page then place the "data" key in the field. Multiple keys must be seperated by newline'
      ),
    ];

    $form['js'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add via javascript'),
      '#description' => $this->t('It will add the parameters via Javascript. This will add the parametres after the page has been rendered.'),
      '#default_value' => $config->get('js'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('carryquery.settings');

    $js = $form_state->getValue('js');
    $data = Html::escape($form_state->getValue('carryqueryconfig'));
    $dataLines = preg_split("/\\r\\n|\\r|\\n/", $data);
    $queryInfo = [];
    $queryKeys = [];
    foreach ($dataLines as $value) {
      $arrPipeSplit = explode('|', $value);
      $queryInfo[$arrPipeSplit[0]] = $arrPipeSplit;
      $queryKeys[] = $arrPipeSplit[0];
    }

    $config->set('carryqueryconfig', $data)
      ->set('js', $js)
      ->set('keys', $queryKeys)
      ->set('info', $queryInfo)
      ->save();

    parent::submitForm($form, $form_state);
  }

}

<?php

namespace Drupal\carryquery;

use Drupal\Core\PathProcessor\PathProcessorManager as CorePathPrcoessorManager;
use Drupal\Core\Render\BubbleableMetadata;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Path processor manager.
 *
 * This class decorates path processor manager service to provide functionality
 * for query carry forwarding.
 */
class PathProcessorManager extends CorePathPrcoessorManager {
  /**
   * Holds the core provided path processor manager.
   *
   * @var Drupal\Core\PathProcessor\PathProcessorManager
   *  Path processor manager
   */
  public $parentPathProcessor;

  /**
   * Current request stack.
   *
   * @var Symfony\Component\HttpFoundation\RequestStack
   *  Request stack.
   */
  public $requestStack;

  /**
   * Constructs path processor manager.
   *
   * @param Drupal\Core\PathProcessor\PathProcessorManager $parentPathProcessor
   *   Core path processor.
   * @param Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Current request stack object.
   */
  public function __construct(CorePathPrcoessorManager $parentPathProcessor, RequestStack $requestStack) {
    $this->parentPathProcessor = $parentPathProcessor;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    if (!$request) {
      // Add request to process outbound for getting the query parameters.
      $request = $this->requestStack->getCurrentRequest();
    }
    return $this->parentPathProcessor->processOutbound($path, $options, $request, $bubbleable_metadata);
  }

}
